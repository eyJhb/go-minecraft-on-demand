package backend

import (
	"context"

	"github.com/digitalocean/godo"
)

type DropletStatus uint8

const (
	DropletStateUnknown DropletStatus = iota
	DropletStateNotCreated
	DropletStateNew
	DropletStateActive
	DropletStateOff
	DropletStateArchive
)

type DropletInfo struct {
	State       DropletStatus
	IPv4        string
	IPv4Private string
}

type Provider struct {
	Region            string
	ImageId           int
	SSHKeyFingerprint string
	VolumeId          string
	DropletSize       string

	Client *godo.Client
}

func (p *Provider) GetInfo(ctx context.Context, name string) (DropletInfo, error) {
	var di DropletInfo

	// get the droplet if possible
	droplet, exists, err := p.internalExists(ctx, name)
	if err != nil {
		return di, err
	}

	// not created, return this
	if !exists {
		di.State = DropletStateNotCreated
		return di, nil
	}

	// created I guess, switch the state
	switch droplet.Status {
	case "new":
		di.State = DropletStateNew
	case "active":
		di.State = DropletStateActive
	case "off":
		di.State = DropletStateOff
	case "archive":
		di.State = DropletStateArchive
	}

	// try to get the IPv4 + IPv4
	ipv4, err := droplet.PublicIPv4()
	if err != nil {
		return di, err
	}

	ipv4private, err := droplet.PrivateIPv4()
	if err != nil {
		return di, err
	}

	di.IPv4 = ipv4
	di.IPv4Private = ipv4private

	return di, nil
}

func (p *Provider) Create(ctx context.Context, name string) error {
	// make request
	cr := &godo.DropletCreateRequest{
		Name:    name,
		Region:  p.Region,
		Image:   godo.DropletCreateImage{ID: p.ImageId},
		SSHKeys: []godo.DropletCreateSSHKey{godo.DropletCreateSSHKey{Fingerprint: p.SSHKeyFingerprint}},
		Volumes: []godo.DropletCreateVolume{godo.DropletCreateVolume{ID: p.VolumeId}},
		Size:    p.DropletSize,

		// extra
		PrivateNetworking: true,
	}

	if _, _, err := p.Client.Droplets.Create(ctx, cr); err != nil {
		return err
	}

	return nil
}

func (p *Provider) Destroy(ctx context.Context, name string) error {
	droplet, exists, err := p.internalExists(ctx, name)
	if err != nil {
		return err
	}

	if !exists {
		return nil
	}

	if _, err := p.Client.Droplets.Delete(ctx, droplet.ID); err != nil {
		return err
	}

	return nil
}

func (p *Provider) Exists(ctx context.Context, name string) (bool, error) {
	_, exists, err := p.internalExists(ctx, name)
	return exists, err
}

func (p *Provider) internalExists(ctx context.Context, name string) (godo.Droplet, bool, error) {
	droplets, err := p.getAll(ctx)
	if err != nil {
		return godo.Droplet{}, false, err
	}

	for _, droplet := range droplets {
		if droplet.Name == name {
			return droplet, true, nil
		}
	}

	return godo.Droplet{}, false, nil
}

func (p *Provider) getAll(ctx context.Context) ([]godo.Droplet, error) {
	var allDroplets []godo.Droplet

	opt := &godo.ListOptions{PerPage: 1000}
	for {
		droplets, resp, err := p.Client.Droplets.List(ctx, opt)
		if err != nil {
			return nil, err
		}

		// append the current page's droplets to our list
		allDroplets = append(allDroplets, droplets...)

		// if we are at the last page, break out the for loop
		if resp.Links == nil || resp.Links.IsLastPage() {
			break
		}

		page, err := resp.Links.CurrentPage()
		if err != nil {
			return nil, err
		}

		// set the page we want for the next request
		opt.Page = page + 1
	}

	return allDroplets, nil
}
