package backend

import (
	"context"
	"errors"
	"strings"
	"sync"

	"github.com/rs/zerolog/log"
	"gitlab.com/eyJhb/go-mcproxy/mcnet/packet"
	"gitlab.com/eyJhb/go-mcproxy/proxy/hooks"
)

var (
	ErrNotWhitelisted = errors.New("you are not whitelisted")
	ErrServerNotReady = errors.New("server is not ready yet")
	ErrCouldNotStart  = errors.New("could not start the server, please try again")
)

const (
	statusMsgFailedToRefresh = "failed to refresh server info, try again."
	statusMsgServerReady     = "server is up, join to play!"
	statusMsgServerDown      = "server is down, join to bring it up"
	statusMsgServerStarting  = "server is starting, please wait and refresh"
)

type Server struct {
	Name string

	Players map[string]struct{}

	// Whitelist key should be lowercase
	Whitelist  map[string]struct{}
	ServerInfo DropletInfo
	Provider   *Provider

	// lock
	sync.Mutex
}

// ConnectPlayer(name string) error
//		- Tries to connect a player, if the server is not started, it will start it if whitelisted
//			- Whitelisted -> start server if not, return error while starting server + change desc
//			- Not whitelisted -> if server is currently on, just allow it, otherwise show error message
func (s *Server) ConnectPlayer(ctx context.Context, username string, hs packet.Handshake, pk *packet.HandshakeMessagePayload) error {
	s.Lock()
	defer s.Unlock()

	log.Info().Str("username", username).Msg("player connected")

	// ensure we have the most up-to-date state
	if err := s.RefreshState(ctx); err != nil {
		log.Error().Err(err).Msg("failed to refresh state")
		return hooks.ErrStopChain
	}

	// server is on, do NOTHING except adding the player
	if s.ServerInfo.State == DropletStateActive {
		s.Players[username] = struct{}{}
		return nil
	}

	// If the state is not on, and the state is not off
	// then we are somewhere in-between
	if s.ServerInfo.State != DropletStateNotCreated {
		pk.Text = ErrServerNotReady.Error()
		return nil
	}

	// Here we are certain to have an off state!!

	// else try to start it, since we have handled all other states
	if !s.isPlayerWhiteListed(username) {
		pk.Text = ErrNotWhitelisted.Error()
		return nil
	}

	// try to start server
	log.Info().Str("username", username).Msg("starting server")
	if err := s.Provider.Create(ctx, s.Name); err != nil {
		log.Error().Err(err).Msg("failed to create server")
		pk.Text = ErrCouldNotStart.Error()
		return nil
	}

	pk.Text = errors.New("starting the server, please wait").Error()

	return nil
}

// DisconnectPlayer(name string)
//		- Remove player from our players dict
func (s *Server) DisconnectPlayer(ctx context.Context, username string) error {
	s.Lock()
	defer s.Unlock()

	log.Info().Str("username", username).Msg("player disconnected")

	// remove the player
	delete(s.Players, username)

	return nil
}

// StatusMinecraft()
//		- the status message that is shown in the minecraft browser
func (s *Server) StatusMinecraft(ctx context.Context, hs packet.Handshake, pk *packet.HandshakeStatusPayload) error {
	s.Lock()
	defer s.Unlock()

	// setup the players
	pk.Players.Max = 100
	pk.Players.Online = len(s.Players)

	if err := s.RefreshState(ctx); err != nil {
		log.Error().Err(err).Msg("failed to refresh state StatusMinecraft")
		pk.Description.Text = statusMsgFailedToRefresh
		return hooks.ErrStopChain
	}

	var msg string
	if s.ServerInfo.State == DropletStateActive {
		msg = statusMsgServerReady
	} else if s.ServerInfo.State == DropletStateNotCreated {
		msg = statusMsgServerDown
	} else {
		msg = statusMsgServerStarting
	}
	pk.Description.Text = msg

	return nil
}

// Generally it is responsible for starting a VPS and taking it down again!
//	- This should be done in a goroutine

// refresh the state of the server
// this function can decide on itself if it has a cache or not
func (s *Server) RefreshState(ctx context.Context) error {
	// get the droplet information
	di, err := s.Provider.GetInfo(ctx, s.Name)

	// always set the new server information
	// so that we do not have stale information
	// ie. old IPv4 addresses
	s.ServerInfo = di

	return err
}

// check if a player is whitelisted
func (s *Server) isPlayerWhiteListed(name string) bool {
	nameLower := strings.ToLower(name)

	if _, ok := s.Whitelist[nameLower]; ok {
		return true
	}

	return false
}
