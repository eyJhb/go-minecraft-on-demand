# Minecraft on demand using Go!

To use this, you need to first create a image using Nix.
This is currently only doable if you have Nix installed, as I have not yet provided a Dockerfile, etc. that can do this.
To do this, you need to do the following, after you have edited the `nix/baseconfig.nix` with your own public SSH key.

```
cd nix
echo "digital-ocean-auth-token" > doauth
NIXPKGS_ALLOW_UNFREE nix-build image.nix
```

Then upload this to DigitalOcean, and get the image id, this can be done using the API, ie. visit `https://cloud.digitalocean.com/api/v1/volumes?sort=created_at` after you have logged in, to get the id.
.

Next start a droplet, and attach a volume to it, ie. like so (stolen from [here](https://github.com/Infinisil/system/blob/b98c7c4bc903a6ff916ff5414cb5bf773c168a45/config/multimods/on-demand-minecraft/readme.txt)).

```
- Create a new DO volume of 1GB size and attach it to your existing droplet
- sudo zpool create -m legacy -o autoexpand=on -O compress=on -O xattr=off -O atime=off minecraft /dev/sda
- Create ops.json if needed
- Create whitelist.json if needed
- chown 114:nogroup the folder recursively
- Load the world into there, or let it autogenerate

- After expanding disk (if necessary), run `sudo zpool online -e minecraft /dev/sda`
```


Create a configuration like below in `~/.config/minecraft-on-demand/config.yml`, with the following content.
```yaml
region: fra1
image_id: 11111111
ssh_key_fingerprint: aa:aa:aa:aa:aa:aa:aa:aa:aa:aa:aa:aa:aa:aa:aa:aa
volume_id: aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee
whitelist: username1,username2,username3
auth_token: <digital-ocean-auth-token>
```
