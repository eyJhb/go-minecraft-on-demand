package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"os"
	"strings"
	"sync"

	"github.com/digitalocean/godo"
	"github.com/rs/zerolog/log"
	"gitlab.com/eyJhb/go-mcproxy/mcnet"
	"gitlab.com/eyJhb/go-mcproxy/mcnet/packet"
	"gitlab.com/eyJhb/go-mcproxy/proxy"
	"gitlab.com/eyJhb/go-mcproxy/proxy/hooks"
	"gitlab.com/eyJhb/go-minecraft-on-demand/backend"
	"gopkg.in/yaml.v2"
)

const (
	configDirPostfix   = "/minecraft-on-demand/config.yml"
	defaultDropletSize = "s-2vcpu-4gb"
	defaultPort        = 25565
)

var (
	flagBind          = flag.String("bind", "0.0.0.0", "specifies the host to bind on")
	flagPort          = flag.Int("port", 25565, "specifies the port to bind on")
	flagAuthTokenFile = flag.String("auth-token-file", "", "specifies the file where the authtoken to digitalocean can be read")
	flagConfig        = flag.String("config", "", "where to read the configuration from, if empty defaults to $XDG_CONFIG_HOME/minecraft-on-demand/config.yml else $HOME/.config/minecraft-on-demand/config.yml")

	flagHumanVersion    = flag.String("releaseversion", proxy.DefaultMinecraftHumanVersion, "what default minecraft release name to use, e.g. 1.17.1")
	flagProtocolVersion = flag.Int("protocolversion", proxy.DefaultMinecraftProtocolVersion, "what default minecraft protocol version to use, e.g. 756")
)

type config struct {
	Region            string `yaml:"region"`
	AuthToken         string `yaml:"auth_token"`
	SSHKeyFingerprint string `yaml:"ssh_key_fingerprint"`

	Servers []confserver `yaml:"servers"`
}

type confserver struct {
	Hostname string `yaml:"hostname"`
	Default  bool   `yaml:"default"`

	ImageId     int    `yaml:"image_id"`
	VolumeId    string `yaml:"volume_id"`
	Whitelist   string `yaml:"whitelist"` // comma seperated list `name1,name2,name3`
	DropletSize string `yaml:"droplet_size"`

	HumanVersion    string `yaml:"human_version"`
	ProtocolVersion int    `yaml:"protocol_version"`
}

type server struct {
	backend    backend.Server
	serverIP   string
	serverPort int
	config     confserver

	sync.Mutex
}

func main() {
	// parse our flags
	flag.Parse()

	// get the config
	conf, err := parseConfig()
	if err != nil {
		log.Error().Err(err).Msg("Failed to get config")
		return
	}

	// init a map of backends
	svcs := make(map[string]*server)
	for _, v := range conf.Servers {
		b := backend.Server{
			Name:      v.Hostname, // TODO(eyJhb): unsure how well this works
			Players:   make(map[string]struct{}),
			Whitelist: parseWhitelist(v.Whitelist),
			Provider: &backend.Provider{
				Region:            conf.Region,
				SSHKeyFingerprint: conf.SSHKeyFingerprint,
				ImageId:           v.ImageId,
				VolumeId:          v.VolumeId,
				DropletSize:       v.DropletSize,

				Client: godo.NewFromToken(conf.AuthToken),
			},
		}

		svcs[v.Hostname] = &server{
			backend:    b,
			serverPort: defaultPort,

			config: v,
		}
	}

	// Listen for incoming connections.
	l, err := net.Listen("tcp", fmt.Sprintf("%s:%d", *flagBind, *flagPort))
	if err != nil {
		log.Error().Err(err).Msg("failed to listen")
		return
	}
	// Close the listener when the application closes.
	defer l.Close()

	fmt.Printf("Listening on %s:%d\n", *flagBind, *flagPort)
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Error().Err(err).Msg("failed to accept connection")
		}

		// proxy client
		p := proxy.Client{
			// pass client connection
			ClientConn: mcnet.WrapConn(conn),
		}

		// our svc
		var svc *server

		// need to intercept browser-status, player-connect and player-disconnect
		browserStatus := func(ctx context.Context, hs packet.Handshake, pk *packet.HandshakeStatusPayload) error {
			if svc == nil {
				svc = svcs[string(hs.Hostname)]

				// if service was not found, then just return this to the user
				if svc == nil {
					pk.Description.Text = "Server with that hostname does not exists"
					return errors.New(pk.Description.Text)
				}
			}

			svc.Lock()
			defer svc.Unlock()
			if err := svc.backend.StatusMinecraft(ctx, hs, pk); err != nil {
				return err
			}
			svc.serverIP = svc.backend.ServerInfo.IPv4
			p.VersionHuman = svc.config.HumanVersion
			p.VersionProtocol = svc.config.ProtocolVersion

			return nil
		}

		playerConnect := func(ctx context.Context, username string, hs packet.Handshake, pk *packet.HandshakeMessagePayload) error {
			if svc == nil {
				svc = svcs[string(hs.Hostname)]

				// if service was not found, then just return this to the user
				if svc == nil {
					pk.Text = "Server with that hostname does not exists"
					return errors.New(pk.Text)
				}
			}

			svc.Lock()
			defer svc.Unlock()
			if err := svc.backend.ConnectPlayer(ctx, username, hs, pk); err != nil {
				return err
			}
			svc.serverIP = svc.backend.ServerInfo.IPv4
			p.VersionHuman = svc.config.HumanVersion
			p.VersionProtocol = svc.config.ProtocolVersion

			log.Info().Str("version_human", p.VersionHuman).Int("version_protocol", p.VersionProtocol).Msg("player connecting, using the following for proxy")

			// if we actually have a IP for the server, then
			// we will setup the socket
			if svc.serverIP != "" {
				tcpAddr, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("%s:%d", svc.serverIP, svc.serverPort))
				if err != nil {
					// always ensure we close the client connection
					conn.Close()

					log.Error().Err(err).Msg("failed to resolve TCP addr")
					return err
				}

				srvConn, err := net.DialTCP("tcp", nil, tcpAddr)
				if err != nil {
					// always ensure we close the client connection
					conn.Close()

					log.Error().Err(err).Msg("failed to initiate connection to server")
					return err
				}

				p.ServerConn = mcnet.WrapConn(srvConn)
			} else {
				pk.Text = "no IP for server, please connect and try again"
				return errors.New("no server IP")
			}

			return nil
		}

		playerDisconnect := func(ctx context.Context, username string) error {
			return svc.backend.DisconnectPlayer(ctx, username)
		}

		// setup hooks
		p.Hooks.Status("browser-status", hooks.Placement{}, browserStatus)
		p.Hooks.Connect("player-connect", hooks.Placement{}, playerConnect)
		p.Hooks.Disconnect("player-disconnect", hooks.Placement{}, playerDisconnect)

		// start in own thread, so that the next connection can be used
		go func() {
			// only report error, if error is not EOF
			if err := p.Start(); err != nil && err != io.EOF {
				log.Error().Err(err).Msg("some error occured when proxying client")
			}
		}()

	}
}

func parseWhitelist(whitelist string) map[string]struct{} {
	whiteSplit := strings.Split(whitelist, ",")

	finalList := make(map[string]struct{})
	for _, entry := range whiteSplit {
		entryTrimmed := strings.ToLower(strings.TrimSpace(entry))
		finalList[entryTrimmed] = struct{}{}
	}

	return finalList
}

func parseConfig() (config, error) {
	// get the path for the config
	configdir := *flagConfig
	if configdir == "" {
		tmpvar, err := os.UserConfigDir()
		if err != nil {
			return config{}, err
		}

		configdir = tmpvar + configDirPostfix
	}

	// try to read the file
	data, err := ioutil.ReadFile(configdir)
	if err != nil {
		return config{}, err
	}

	var conf config
	if err := yaml.Unmarshal(data, &conf); err != nil {
		return config{}, err
	}

	// set defaults
	//// defaults for servers
	var hasDefault bool
	for i, v := range conf.Servers {
		if v.HumanVersion == "" {
			conf.Servers[i].HumanVersion = *flagHumanVersion
		}
		if v.ProtocolVersion == 0 {
			conf.Servers[i].ProtocolVersion = *flagProtocolVersion
		}

		if v.DropletSize == "" {
			conf.Servers[i].DropletSize = defaultDropletSize
		}

		if v.Default {
			if hasDefault {
				return conf, errors.New("there can only be one default server")
			}

			hasDefault = true
		}
	}

	// set default if none is set
	if !hasDefault && len(conf.Servers) > 0 {
		conf.Servers[0].Default = true
	}

	//// set the authtokenfile
	if flagAuthTokenFile != nil && *flagAuthTokenFile != "" {
		authTokenBytes, err := ioutil.ReadFile(*flagAuthTokenFile)
		if err != nil {
			log.Error().Err(err).Msg("failed to read authtoken file")
			return conf, err
		}
		conf.AuthToken = string(authTokenBytes)
	}

	return conf, nil
}
