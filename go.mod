module gitlab.com/eyJhb/go-minecraft-on-demand

go 1.17

require (
	github.com/digitalocean/godo v1.74.0
	github.com/rs/zerolog v1.26.1
	gitlab.com/eyJhb/go-mcproxy v0.0.0-20220121205747-6c7424e6457f
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	golang.org/x/net v0.0.0-20220121210141-e204ce36a2ba // indirect
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
