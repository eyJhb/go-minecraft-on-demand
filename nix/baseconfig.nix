{ config, lib, pkgs, ... }:

# remember to create a doauth file, which has a token for digitalocean
let 
  sshkey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJHuxYiiGVJekQWOu3VjR1ZI1LlJ65tq+5omw2OLb8cI eyjhb@eos";
in {
  imports = [
    ./modules/minecraft-module.nix
  ];

  # data dir for minecraft, etc.
  boot.supportedFilesystems = ["zfs"];
  fileSystems."/var/lib/minecraft" = {
    device = "minecraft";
    fsType = "zfs";
  };
  networking.hostId = "8e4b0e65";

  systemd.services.destruct = {
    path = [ pkgs.curl ];
    script = ''
      set -xu
      systemctl stop minecraft-server
      /run/booted-system/sw/bin/zpool export minecraft
      id=$(curl http://169.254.169.254/metadata/v1/id)
      curl -X DELETE -H "Content-Type: application/json" -H "Authorization: Bearer ${lib.fileContents ./secrets/doauth}" "https://api.digitalocean.com/v2/droplets/$id"
    '';
  };

  systemd.services.self-destruct = {
    wantedBy = [ "multi-user.target" ];
    path = [ pkgs.iproute pkgs.curl pkgs.gawk ];
    script = ''
      set -xu
      count=0
      while [ $count -lt 5 ]; do
        sleep 60

        # do not prematurely turn off the server, let it try to run out the hour
        HOURSPEND=$(expr $(( ($(awk -F. '{printf $1}' /proc/uptime) / 60) % 60)))
        if [ "$HOURSPEND" -le 50 ]; then
            count=0
            continue
        fi

        if [ -z "$(ss -tOH 'sport = 25565')" ]; then
          count=$(( count + 1 ))
          echo "Incrementing count to $count"
        else
          count=0
        fi
      done
      echo "Initiating self-destruct"
      systemctl start destruct
    '';
  };

  # user stuff + openssh
  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = [
    sshkey
  ];

  # general nix stuff
  nixpkgs.config.allowUnfree = true;
  # nix.autoOptimiseStore = true;

  # not really needed to be changed
  system.stateVersion = "21.05";
  system.nixos.revision = "unknown";

  # save space by disabling/minimising this
  i18n.supportedLocales = [ (config.i18n.defaultLocale + "/UTF-8") ];
  documentation.enable = lib.mkDefault false;
  documentation.nixos.enable = lib.mkDefault false;
  xdg.mime.enable = false;
  security.polkit.enable = false;
  services.udisks2.enable = false;
  services.zfs.zed.settings.ZED_EMAIL_PROG = "mail";
  nixpkgs.overlays = [(self: super: {
    zfs = super.zfs.override { enablePython = false; };
  })];
}
