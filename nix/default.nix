# { pkgs ? import <nixpkgs> { config.allowUnfree = true; } }:
let
  nixpkgs = import ./nixpkgs.nix;
  pkgs = import nixpkgs {};
  mkConfig = configFile: {
    imports = [
      "${nixpkgs}/nixos/modules/virtualisation/digital-ocean-image.nix"
      # <nixpkgs/nixos/modules/virtualisation/digital-ocean-image.nix>
      ./baseconfig.nix

      configFile
    ];

    virtualisation.digitalOceanImage.compressionMethod = "bzip2";
    virtualisation.digitalOceanImage.diskSize = 8*1024;
  };
in {
  vanilla = (pkgs.nixos (mkConfig ./configs/vanilla.nix)).digitalOceanImage;
  techreborn = (pkgs.nixos (mkConfig ./configs/techreborn.nix)).digitalOceanImage;
}
