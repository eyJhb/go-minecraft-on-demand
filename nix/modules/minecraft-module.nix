{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.minecraft-server;

  # ops submodule
  opOpts = { name, config, ... }: {
    options = {
      name = mkOption {
        type = types.str;
        default = name;
        description = "name of the use to give op, defaults to the attribute name.";
      };
      level = mkOption {
        type = types.enum [ 1 2 3 4 ];
        description = "op-level to give the user";
      };
      uuid = mkOption {
        type = types.str;
        description = "the uuid4 of the user added";
      };
      bypassesPlayerLimit = mkOption {
        type = types.bool;
        default = false;
        description = "sets the bypassesplayerlimit";
      };
    };
  };

  # takes a attrset of ops
  genOps = ops: pkgs.writeText "ops.json" (builtins.toJSON (mapAttrsToList (n: v: v) ops));
in {
  options.services.minecraft-server = {
    mods = mkOption {
      type = with types; listOf path;
      default = [];
    };

    modsFolder = mkOption {
      type = with types; str;
      default = "mods";
    };

    ops = mkOption {
      type = with types; nullOr (attrsOf (submodule opOpts));
      default = null;
      description = "manage ops.json, if declarative will delete exsisting ops.json. Otherwise will only create the file, if it does not exists.";
    };
  };

  config = mkIf cfg.enable {
    systemd.services.minecraft-server.preStart = mkAfter ''
      # create mods folder
      mkdir -p ${cfg.modsFolder}

      # delete any symbolic links in mods folder
      find ${cfg.modsFolder} -type l -delete

      # symbolic link our mods
      ${concatStringsSep "\n" (map (v: "ln -sf ${v} ${cfg.modsFolder}/") cfg.mods)}

      ${if cfg.declarative then ''
      # if declarative, delete ops.json
      rm -f ops.json''
       else ""}

      ${if (cfg.ops != null) then ''
      # if our ops.json does not exists, create it 
      ln -sf ${genOps cfg.ops} ops.json
      '' else ""}
    ''; 
  };
}
