{ pkgs, lib, ... }:

let
  serverjar = pkgs.fetchurl {
    url = "https://meta.fabricmc.net/v2/versions/loader/1.18.1/0.12.12/0.10.2/server/jar";
    sha256 = "sha256-rwY95tJdirstBMaSaZ9mfNwp2dQ4XWx77/ocSghROn0=";
  };

  mods = {
    fabric-api = pkgs.fetchurl {
      name = "fabric-api.jar";
      url = "https://media.forgecdn.net/files/3609/610/fabric-api-0.46.1%2b1.18.jar";
      sha256 = "sha256-kEKO0kPdml2fo6uNtoDrMdwwq3js+pj8CsX7rGnizTQ=";
    };
    techreborn = pkgs.fetchurl {
      name = "techreborn.jar";
      url = "https://media.forgecdn.net/files/3597/223/TechReborn-5.1.0-beta.5.jar";
      sha256 = "sha256-lGtBt6YhMxQZuSVpUU5GdH2QmD9KTZk4xFcrkgP0KRE=";
    };
  };
in {
  # minecraft things!
  services.minecraft-server = {
    enable = true;
    eula = true;
    openFirewall = true;
    declarative = true;

    package = pkgs.minecraft-server.overrideAttrs (old: { src = serverjar; });

    serverProperties = {
      difficulty = "hard";
      gamemode = "survivial";
    };

    mods = [
      mods.fabric-api
      mods.techreborn
    ];

    ops = {
      "johan123jo" = {
        level = 4;
        uuid = "e171a7d4-093e-47d6-b71b-7015a5f97c57";
      };
      "NuzzGuzz" = {
        level = 2;
        uuid = "db509a3f-8e4e-4f47-bbfd-6901aab8821f";
      };
    };

    jvmOpts = lib.concatStringsSep " " [
      "-Xms2G"
      "-Xmx3G"
    ];
  };
}
