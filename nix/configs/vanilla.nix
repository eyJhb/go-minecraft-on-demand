{ pkgs, lib, ... }:

let
  serverjar = (import ./mcversions.nix {}).minecraft_1_19_2;
  # serverjar = pkgs.fetchurl {
  #   url = "https://piston-data.mojang.com/v1/objects/f69c284232d7c7580bd89a5a4931c3581eae1378/server.jar";
  #   sha256 = "sha256-smcnBp719hxwSt2aN4rJDj0nH9eHbAvT3Pvp/QvsTZY=";
  # };
in {
  # minecraft things!
  services.minecraft-server = {
    enable = true;
    eula = true;
    openFirewall = true;
    declarative = true;

    package = pkgs.minecraft-server.overrideAttrs (old: { src = serverjar; });

    serverProperties = {
      difficulty = "hard";
      gamemode = "survivial";
    };

    ops = {
      "johan123jo" = {
        level = 4;
        uuid = "e171a7d4-093e-47d6-b71b-7015a5f97c57";
      };
      "NuzzGuzz" = {
        level = 2;
        uuid = "db509a3f-8e4e-4f47-bbfd-6901aab8821f";
      };
    };

    jvmOpts = lib.concatStringsSep " " [
      "-Xms2G"
      "-Xmx4G"
    ];
  };
}
