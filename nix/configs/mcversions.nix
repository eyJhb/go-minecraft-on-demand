{ pkgs ? import <nixpkgs> {}, ... }:

let
  mkURL= sha1: "https://piston-data.mojang.com/v1/objects/${sha1}/server.jar";
  fetchMCVersion = sha1: pkgs.fetchurl {
    url = mkURL sha1;
    sha1 = sha1;
  };
in {
  minecraft_1_19_2 = fetchMCVersion "f69c284232d7c7580bd89a5a4931c3581eae1378";
}
