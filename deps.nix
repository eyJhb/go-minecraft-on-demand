# file generated from go.mod using vgo2nix (https://github.com/nix-community/vgo2nix)
[
  {
    goPackagePath = "cloud.google.com/go";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/google-cloud-go";
      rev = "v0.65.0";
      sha256 = "0xrcg39jrw90gq8vl8pfq63sp3bkak04wh392yj5rh4lc1a4lxjq";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "cloud.google.com/go/bigquery";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/google-cloud-go";
      rev = "bigquery/v1.8.0";
      sha256 = "1127ha4r0xjsfl04mdb134b6kvpc6yz5bx4bba8m1jmb4k3vyg3j";
      moduleDir = "bigquery";
    };
  }
  {
    goPackagePath = "cloud.google.com/go/datastore";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/google-cloud-go";
      rev = "datastore/v1.1.0";
      sha256 = "18f1l28665x1a8j8a5bh2i7wb2vrwj050d1g5qda50isgqaybixd";
      moduleDir = "datastore";
    };
  }
  {
    goPackagePath = "cloud.google.com/go/pubsub";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/google-cloud-go";
      rev = "pubsub/v1.3.1";
      sha256 = "1fxsj63d773yf6mjas5gwsq2caa6iqxmss6mms0yfdcc6krg6zkf";
      moduleDir = "pubsub";
    };
  }
  {
    goPackagePath = "cloud.google.com/go/storage";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/google-cloud-go";
      rev = "storage/v1.10.0";
      sha256 = "10fp6galzz8jwx35159xdcrwsqaz95xw78iwv1z5n67vhglwi5nf";
      moduleDir = "storage";
    };
  }
  {
    goPackagePath = "dmitri.shuralyov.com/gpu/mtl";
    fetch = {
      type = "git";
      url = "https://dmitri.shuralyov.com/gpu/mtl";
      rev = "666a987793e9";
      sha256 = "1isd03hgiwcf2ld1rlp0plrnfz7r4i7c5q4kb6hkcd22axnmrv0z";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/BurntSushi/toml";
    fetch = {
      type = "git";
      url = "https://github.com/BurntSushi/toml";
      rev = "v0.3.1";
      sha256 = "1fjdwwfzyzllgiwydknf1pwjvy49qxfsczqx5gz3y0izs7as99j6";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/BurntSushi/xgb";
    fetch = {
      type = "git";
      url = "https://github.com/BurntSushi/xgb";
      rev = "27f122750802";
      sha256 = "18lp2x8f5bljvlz0r7xn744f0c9rywjsb9ifiszqqdcpwhsa0kvj";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/census-instrumentation/opencensus-proto";
    fetch = {
      type = "git";
      url = "https://github.com/census-instrumentation/opencensus-proto";
      rev = "v0.2.1";
      sha256 = "19fcx3sc99i5dsklny6r073z5j20vlwn2xqm6di1q3b1xwchzqfj";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/chzyer/logex";
    fetch = {
      type = "git";
      url = "https://github.com/chzyer/logex";
      rev = "v1.1.10";
      sha256 = "08pbjj3wx9acavlwyr055isa8a5hnmllgdv5k6ra60l5y1brmlq4";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/chzyer/readline";
    fetch = {
      type = "git";
      url = "https://github.com/chzyer/readline";
      rev = "2972be24d48e";
      sha256 = "104q8dazj8yf6b089jjr82fy9h1g80zyyzvp3g8b44a7d8ngjj6r";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/chzyer/test";
    fetch = {
      type = "git";
      url = "https://github.com/chzyer/test";
      rev = "a1ea475d72b1";
      sha256 = "0rns2aqk22i9xsgyap0pq8wi4cfaxsri4d9q6xxhhyma8jjsnj2k";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/client9/misspell";
    fetch = {
      type = "git";
      url = "https://github.com/client9/misspell";
      rev = "v0.3.4";
      sha256 = "1vwf33wsc4la25zk9nylpbp9px3svlmldkm0bha4hp56jws4q9cs";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/cncf/udpa/go";
    fetch = {
      type = "git";
      url = "https://github.com/cncf/udpa";
      rev = "269d4d468f6f";
      sha256 = "0i1jiaw2k3hlwwmg4hap81vb4s1p25xp9kdfww37v0fbgjariccs";
      moduleDir = "go";
    };
  }
  {
    goPackagePath = "github.com/coreos/go-systemd/v22";
    fetch = {
      type = "git";
      url = "https://github.com/coreos/go-systemd";
      rev = "v22.3.2";
      sha256 = "1ndi86b8va84ha93njqgafypz4di7yxfd5r5kf1r0s3y3ghcjajq";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/davecgh/go-spew";
    fetch = {
      type = "git";
      url = "https://github.com/davecgh/go-spew";
      rev = "v1.1.0";
      sha256 = "0d4jfmak5p6lb7n2r6yvf5p1zcw0l8j74kn55ghvr7zr7b7axm6c";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/digitalocean/godo";
    fetch = {
      type = "git";
      url = "https://github.com/digitalocean/godo";
      rev = "v1.74.0";
      sha256 = "16qhgl3dmsgi7ay60569qh7h454wwmacsfazz5qnhfq5za6p90vy";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/envoyproxy/go-control-plane";
    fetch = {
      type = "git";
      url = "https://github.com/envoyproxy/go-control-plane";
      rev = "v0.9.4";
      sha256 = "0m0crzx70lp7vz13v20wxb1fcfdnzp7h3mkh3bn6a8mbfz6w5asj";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/envoyproxy/protoc-gen-validate";
    fetch = {
      type = "git";
      url = "https://github.com/envoyproxy/protoc-gen-validate";
      rev = "v0.1.0";
      sha256 = "0kxd3wwh3xwqk0r684hsy281xq4y71cd11d4q2hspcjbnlbwh7cy";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/go-gl/glfw";
    fetch = {
      type = "git";
      url = "https://github.com/go-gl/glfw";
      rev = "e6da0acd62b1";
      sha256 = "0prvx5r7q8yrhqvnwibv4xz3dayjbq36yajzqvh0z4lqsh4hyhch";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/go-gl/glfw/v3.3/glfw";
    fetch = {
      type = "git";
      url = "https://github.com/go-gl/glfw";
      rev = "6f7a984d4dc4";
      sha256 = "1nyv7h08qf4dp8w9pmcnrc6vv9bkwj8fil6pz0mkbss5hf4i8xcq";
      moduleDir = "v3.3/glfw";
    };
  }
  {
    goPackagePath = "github.com/godbus/dbus/v5";
    fetch = {
      type = "git";
      url = "https://github.com/godbus/dbus";
      rev = "v5.0.4";
      sha256 = "0znax8kskb5gmp5fj75w56bc9p7b22wrdswzlh4d04sprlc471yi";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/golang/glog";
    fetch = {
      type = "git";
      url = "https://github.com/golang/glog";
      rev = "23def4e6c14b";
      sha256 = "0jb2834rw5sykfr937fxi8hxi2zy80sj2bdn9b3jb4b26ksqng30";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/golang/groupcache";
    fetch = {
      type = "git";
      url = "https://github.com/golang/groupcache";
      rev = "8c9f03a8e57e";
      sha256 = "0vjjr79r32icjzlb05wn02k59av7jx0rn1jijml8r4whlg7dnkfh";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/golang/mock";
    fetch = {
      type = "git";
      url = "https://github.com/golang/mock";
      rev = "v1.4.4";
      sha256 = "1lj0dvd6div4jaq1s0afpwqaq9ah8cxhkq93wii2ably1xmp2l0a";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/golang/protobuf";
    fetch = {
      type = "git";
      url = "https://github.com/golang/protobuf";
      rev = "v1.5.2";
      sha256 = "1mh5fyim42dn821nsd3afnmgscrzzhn3h8rag635d2jnr23r1zhk";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/google/btree";
    fetch = {
      type = "git";
      url = "https://github.com/google/btree";
      rev = "v1.0.0";
      sha256 = "0ba430m9fbnagacp57krgidsyrgp3ycw5r7dj71brgp5r52g82p6";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/google/go-cmp";
    fetch = {
      type = "git";
      url = "https://github.com/google/go-cmp";
      rev = "v0.5.5";
      sha256 = "12fmkdhyv5d4is8s57k78j097zb0phlgnrkqc03agiszxlid69x7";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/google/go-querystring";
    fetch = {
      type = "git";
      url = "https://github.com/google/go-querystring";
      rev = "v1.1.0";
      sha256 = "15k460c23nsmqd1nx3mvrnazws8bpb1gafrmffx7vf91m200mnwa";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/google/martian";
    fetch = {
      type = "git";
      url = "https://github.com/google/martian";
      rev = "v2.1.0";
      sha256 = "197hil6vrjk50b9wvwyzf61csid83whsjj6ik8mc9r2lryxlyyrp";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/google/martian/v3";
    fetch = {
      type = "git";
      url = "https://github.com/google/martian";
      rev = "v3.0.0";
      sha256 = "0ngvs4l6a06s8scnliq9i5pwvwpzfpa5wf9skg0gcvysli1rm3fm";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/google/pprof";
    fetch = {
      type = "git";
      url = "https://github.com/google/pprof";
      rev = "1a94d8640e99";
      sha256 = "0isfwwan9vimwsfjml0phwrqv6kb83mkmk063dcb81k09gzzs3wy";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/google/renameio";
    fetch = {
      type = "git";
      url = "https://github.com/google/renameio";
      rev = "v0.1.0";
      sha256 = "1ki2x5a9nrj17sn092d6n4zr29lfg5ydv4xz5cp58z6cw8ip43jx";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/googleapis/gax-go/v2";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/gax-go";
      rev = "v2.0.5";
      sha256 = "1lxawwngv6miaqd25s3ba0didfzylbwisd2nz7r4gmbmin6jsjrx";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/hashicorp/golang-lru";
    fetch = {
      type = "git";
      url = "https://github.com/hashicorp/golang-lru";
      rev = "v0.5.1";
      sha256 = "13f870cvk161bzjj6x41l45r5x9i1z9r2ymwmvm7768kg08zznpy";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/ianlancetaylor/demangle";
    fetch = {
      type = "git";
      url = "https://github.com/ianlancetaylor/demangle";
      rev = "5e5cf60278f6";
      sha256 = "1fhjk11cip9c3jyj1byz9z77n6n2rlxmyz0xjx1zpn1da3cvri75";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/jstemmer/go-junit-report";
    fetch = {
      type = "git";
      url = "https://github.com/jstemmer/go-junit-report";
      rev = "v0.9.1";
      sha256 = "1knip80yir1cdsjlb3rzy0a4w3kl4ljpiciaz6hjzwqlfhnv7bkw";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/kisielk/gotool";
    fetch = {
      type = "git";
      url = "https://github.com/kisielk/gotool";
      rev = "v1.0.0";
      sha256 = "14af2pa0ssyp8bp2mvdw184s5wcysk6akil3wzxmr05wwy951iwn";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/kr/pretty";
    fetch = {
      type = "git";
      url = "https://github.com/kr/pretty";
      rev = "v0.1.0";
      sha256 = "18m4pwg2abd0j9cn5v3k2ksk9ig4vlwxmlw9rrglanziv9l967qp";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/kr/pty";
    fetch = {
      type = "git";
      url = "https://github.com/kr/pty";
      rev = "v1.1.1";
      sha256 = "0383f0mb9kqjvncqrfpidsf8y6ns5zlrc91c6a74xpyxjwvzl2y6";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/kr/text";
    fetch = {
      type = "git";
      url = "https://github.com/kr/text";
      rev = "v0.1.0";
      sha256 = "1gm5bsl01apvc84bw06hasawyqm4q84vx1pm32wr9jnd7a8vjgj1";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/pkg/errors";
    fetch = {
      type = "git";
      url = "https://github.com/pkg/errors";
      rev = "v0.9.1";
      sha256 = "1761pybhc2kqr6v5fm8faj08x9bql8427yqg6vnfv6nhrasx1mwq";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/pmezard/go-difflib";
    fetch = {
      type = "git";
      url = "https://github.com/pmezard/go-difflib";
      rev = "v1.0.0";
      sha256 = "0c1cn55m4rypmscgf0rrb88pn58j3ysvc2d0432dp3c6fqg6cnzw";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/prometheus/client_model";
    fetch = {
      type = "git";
      url = "https://github.com/prometheus/client_model";
      rev = "14fe0d1b01d4";
      sha256 = "0zdmk6rbbx39cvfz0r59v2jg5sg9yd02b4pds5n5llgvivi99550";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/rogpeppe/go-internal";
    fetch = {
      type = "git";
      url = "https://github.com/rogpeppe/go-internal";
      rev = "v1.3.0";
      sha256 = "0mcdh1licgnnahwml9y2iq6xy5x9xmjw5frcnds2s3wpjyqrl216";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/rs/xid";
    fetch = {
      type = "git";
      url = "https://github.com/rs/xid";
      rev = "v1.3.0";
      sha256 = "0w2hva6ymn16yn6zrwb6nx3kxaffva95w7gj2fwg0xx39fyfslbb";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/rs/zerolog";
    fetch = {
      type = "git";
      url = "https://github.com/rs/zerolog";
      rev = "v1.26.1";
      sha256 = "027fwbaavn58h94053rzwv9y42jvil4jfdjppq10vjw0qq0q4q04";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/stretchr/objx";
    fetch = {
      type = "git";
      url = "https://github.com/stretchr/objx";
      rev = "v0.1.0";
      sha256 = "19ynspzjdynbi85xw06mh8ad5j0qa1vryvxjgvbnyrr8rbm4vd8w";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/stretchr/testify";
    fetch = {
      type = "git";
      url = "https://github.com/stretchr/testify";
      rev = "v1.4.0";
      sha256 = "187i5g88sxfy4vxpm7dw1gwv29pa2qaq475lxrdh5livh69wqfjb";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/yuin/goldmark";
    fetch = {
      type = "git";
      url = "https://github.com/yuin/goldmark";
      rev = "v1.4.0";
      sha256 = "0n3xhna2rnxixjy5d7gfvgjkhddppa8hpzd8i8b68yb2l016g3qk";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "gitlab.com/eyJhb/go-mcproxy";
    fetch = {
      type = "git";
      url = "https://gitlab.com/eyJhb/go-mcproxy.git";
      rev = "6c7424e6457f";
      sha256 = "070bb2in8sxsqakcdvp4av58lz4g1ighvnxqpbmbw8z587rzvxpl";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "go.opencensus.io";
    fetch = {
      type = "git";
      url = "https://github.com/census-instrumentation/opencensus-go";
      rev = "v0.22.4";
      sha256 = "10skbvs4yxjm3z10yfg5cj2mdzjsna87qk1icp9xv0iwp8wzq6h0";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/crypto";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/crypto";
      rev = "cf75a172585e";
      sha256 = "08nvmzqxpy53avnnr6i2vbixg45wbsx44831l96xsfc9slv6w8kf";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/exp";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/exp";
      rev = "6cc2880d07d6";
      sha256 = "1iia6hiif6hcp0cg1i6nq63qg0pmvm2kq24pf2r2il3597rfmlgy";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/image";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/image";
      rev = "cff245a6509b";
      sha256 = "0hiznlkiaay30acwvvyq8g6bm32r7bc6gv47pygrcxqpapasbz84";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/lint";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/lint";
      rev = "738671d3881b";
      sha256 = "0jkiz4py59jjnkyxbxifpf7bsar11lbgmj5jiq2kic5k03shkn9c";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/mobile";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/mobile";
      rev = "d2bd2a29d028";
      sha256 = "1nv6vvhnjr01nx9y06q46ww87dppdwpbqrlsfg1xf2587wxl8xiv";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/mod";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/mod";
      rev = "v0.4.2";
      sha256 = "0z1p17i291z2k6va08i80ljyzyij2b6qsz78lnz0wi8zsnkfdz43";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/net";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/net";
      rev = "e204ce36a2ba";
      sha256 = "064pqlg9499ki0xx92gxql1hmhmnhl04ijha8z1jiffm5lgk7bp5";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/oauth2";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/oauth2";
      rev = "d3ed0bb246c8";
      sha256 = "1vwkvx9kicl0sx27a41r5nfhaw3r5ni94xrvdg5mdihb0g57skwq";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/sync";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/sync";
      rev = "036812b2e83c";
      sha256 = "1gl202py3s4gl6arkaxlf8qa6f0jyyg2f95m6f89qnfmr416h85b";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/sys";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/sys";
      rev = "1d35b9e2eb4e";
      sha256 = "09xmnw6hhpqnakm99xxigg0znbx46f084lpacz67p5rbcdngjxis";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/term";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/term";
      rev = "03fcf44c2211";
      sha256 = "0aw5lgwq5w5kvwfa3jl7l83p9c827ksy4a99dqzzsqxvmk2zdi8f";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/text";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/text";
      rev = "v0.3.7";
      sha256 = "0xkw0qvfjyifdqd25y7nxdqkdh92inymw3q7841nricc9s01p4jy";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/time";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/time";
      rev = "555d28b269f0";
      sha256 = "1rhl4lyz030kwfsg63yk83yd3ivryv1afmzdz9sxbhcj84ym6h4r";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/tools";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/tools";
      rev = "v0.1.7";
      sha256 = "0nn8m2j5vz6p6azq2m5yn20qi0m8n5m439ihk7aabf6lc4lpx4g9";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/xerrors";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/xerrors";
      rev = "5ec99f83aff1";
      sha256 = "1dbzc3gmf2haazpv7cgmv97rq40g2xzwbglc17vas8dwhgwgwrzb";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "google.golang.org/api";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/google-api-go-client";
      rev = "v0.30.0";
      sha256 = "1vh5rrc6hvzh1rqawrwr4nyilnr3dgqx1pr2njjzdg0prfmflm5p";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "google.golang.org/appengine";
    fetch = {
      type = "git";
      url = "https://github.com/golang/appengine";
      rev = "v1.6.7";
      sha256 = "1wkipg7xxc0ha5p6c3bj0vpgq38l18441n5l6zxdhx0gzvz5z1hs";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "google.golang.org/genproto";
    fetch = {
      type = "git";
      url = "https://github.com/googleapis/go-genproto";
      rev = "8632dd797987";
      sha256 = "0h8s1mz8pbdiwmv2g61afa7f05zin3m6mdy2fmny6wcidz4w2i3k";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "google.golang.org/grpc";
    fetch = {
      type = "git";
      url = "https://github.com/grpc/grpc-go";
      rev = "v1.31.0";
      sha256 = "1y3lilpipmaysjk4skbzw1qa6lv05vzixzw9lg9x73xfb01q4nyz";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "google.golang.org/protobuf";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/protobuf";
      rev = "v1.27.1";
      sha256 = "0aszb7cv8fq1m8akgd4kjyg5q7g5z9fdqnry6057ygq9r8r2yif2";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "gopkg.in/check.v1";
    fetch = {
      type = "git";
      url = "https://gopkg.in/check.v1";
      rev = "788fd7840127";
      sha256 = "0v3bim0j375z81zrpr5qv42knqs0y2qv2vkjiqi5axvb78slki1a";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "gopkg.in/errgo.v2";
    fetch = {
      type = "git";
      url = "https://gopkg.in/errgo.v2";
      rev = "v2.1.0";
      sha256 = "065mbihiy7q67wnql0bzl9y1kkvck5ivra68254zbih52jxwrgr2";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "gopkg.in/yaml.v2";
    fetch = {
      type = "git";
      url = "https://gopkg.in/yaml.v2";
      rev = "v2.4.0";
      sha256 = "1pbmrpj7gcws34g8vwna4i2nhm9p6235piww36436xhyaa10cldr";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "honnef.co/go/tools";
    fetch = {
      type = "git";
      url = "https://github.com/dominikh/go-tools";
      rev = "v0.0.1-2020.1.4";
      sha256 = "182j3zzx1bj4j4jiamqn49v9nd3vcrx727f7i9zgcrgmiscvw3mh";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "rsc.io/binaryregexp";
    fetch = {
      type = "git";
      url = "https://github.com/rsc/binaryregexp";
      rev = "v0.2.0";
      sha256 = "1kar0myy85waw418zslviwx8846zj0m9cmqkxjx0fvgjdi70nc4b";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "rsc.io/quote/v3";
    fetch = {
      type = "git";
      url = "https://github.com/rsc/quote";
      rev = "v3.1.0";
      sha256 = "0nvv97hwwrl1mx5gzsbdm1ndnwpg3m7i2jb10ig9wily7zmvki0i";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "rsc.io/sampler";
    fetch = {
      type = "git";
      url = "https://github.com/rsc/sampler";
      rev = "v1.3.0";
      sha256 = "0byxk2ynba50py805kcvbvjzh59l1r308i1xgyzpw6lff4xx9xjh";
      moduleDir = "";
    };
  }
]
