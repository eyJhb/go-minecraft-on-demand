{ pkgs ? import <nixpkgs> {} }:

pkgs.buildGoPackage rec {
  pname = "go-minecraft-on-demand";
  version = "v0.0.3";
  goPackagePath = "gitlab.com/eyJhb/go-minecraft-on-demand";
  src = ./.;  # Incomplete
  goDeps = ./deps.nix;
}
